#!/bin/bash

file_path="/etc/os-release"
source_content=$(<$file_path)
name=$(grep -oP '(?<=^NAME=").*(?=")' <<< "$source_content")

echo "Hello world from \"$name\""